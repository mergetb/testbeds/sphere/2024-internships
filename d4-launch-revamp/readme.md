# Launch Revamp

## Overview

The main web user interface for SPHERE (and what it's built upon, Merge) is called Launch.
No one on this team claims that they're a UX designer (nor is there a lot of time to think deeply about this), especially during the creation of Launch.

The goal of this project then, is to investigate and ideally implement ways to "improve" the Launch UI.
Improve here, could mean a number of different things:
 - Does the UI "make sense"?
 - Are there goofed up "UI bugs" in Launch?
 - Are there better ways of communicating the system to the user?
 - When user executes a specific workflow, comprising of multiple individual tasks:
  - Is the interface clear enough that for whatever next task the user needs to do, they can find it?
  - Does the interface clearly communicate the status of any given task?
  - How optimized is the interface to execute the workflow (as in like, how many clicks does it take to do, how much mouse movement is required, etc, etc.)
  - Something something does it follow something something UI principles (I dunno, I'm just a code monkey)

These workflows can be from a variety of perspectives, aka, the tasks that an experimenter would do is somewhat different than an operator, and different again than a developer.

There's also a need to bump the versions of patternfly and react, as we are behind quite a bit.

## Tasks

- [ ] Bootstrapping things (accounts, access to development resources, etc.)
- [ ] Do an initial read of the UI and take experiential notes
- [ ] Identify and investigate workflows, ranked by frequency
- [ ] Ideate and design improvements the workflow
- [ ] Implement the improvements to the workflow
- [ ] Familiarizing yourself with the launch code and its dependencies
      - There's a bunch of subtasks in here (building the code, deploying it, testing it, etc.)
- [ ] Update patternfly/react

## Documentation

* [Launch Source Code](https://gitlab.com/mergetb/portal/launch)
  - Dependencies:
    - [React](https://react.dev/)
    - [PatternFly](https://v4-archive.patternfly.org/v4). Note we use an older version, v4.
    - The [Merge API](https://mergetb.gitlab.io/api/)
* [Merge Source Code](https://gitlab.com/mergetb/portal/services)

## Contact Info
christra@isi.edu
christran on mattermost
