# Automated GUI testing 

## Overview

The SPHERE deployment of the Merge Testbed software has a single graphical frontend called Launch. While the merge system itself has some built-in automated 
testing, the graphical user interface (GUI) of Launch has none. This task is to add that. There are two parts to the task - writing the tests themselves and 
then integration into the Merge continuous integration and deployment (CI/CD) system. The end result of this should be that when a developer makes a change to 
the code base of Launch and pushes this change, the tests you've written will be automatically run over the code. And if the updates break something, 
we will know before the new (buggy) version is deployed. 

(An alternate task may be to update Launch to the newest versions of tools and packages we use. Let me know if this appeals to you more.)

## Tasks

- [ ] Familiarize yourself with the [Launch code base](https://gitlab.com/mergetb/portal/launch)
  - [ ] Familiarize yourself with the libraries and toolkits that Launch uses
     - [ ] [React](https://react.dev/)
     - [ ] [PatternFly](https://v4-archive.patternfly.org/v4). Note we use an older version, v4.
     - [ ] The [Merge API](https://mergetb.gitlab.io/api/)
- [ ] Review existing GUI testing suites or packages
- [ ] Choose a package to use
- [ ] Write tests using this package against Launch

- [ ] Integrate these tests into the CI chain that we use to build (and now test!) Launch. 
    - [ ] Familiarize yourself with the existing CI chain. We use [Gitlab CI](https://docs.gitlab.com/ee/ci/)
    - [ ] Figure out how to add the GUI testing to this.

## Documentation

* [Gitlab CI](https://docs.gitlab.com/ee/ci/)
* [Launch Source Code](https://gitlab.com/mergetb/portal/launch)
* [Merge Source Code](https://gitlab.com/mergetb/portal/services)
* [Merge API](https://mergetb.gitlab.io/api/)

## Contact Info
glawler@isi.edu
glawler on Mattermost
