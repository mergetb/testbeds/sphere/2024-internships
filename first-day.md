# First day tasks

- create account on launch.mod.deterlab.net
- join internships organization
- join in2024 project
- create your first experiment by following Hello World (Web Interface) example [here](https://mergetb.org/docs/experimentation/)
- connect to your experiment using SSH and ping from a to c
- extend the experiment with fourth node so that you have a-b-c topology and a node d, attached to b

- intall mrg CLI
- delete your first experiment and your XDC
- use CLI to
    - create an XDC
    - create an experiment in project in2024
    - push a two-node topology to the experiment
    - realize and materialize
    - connect between XDC and experiment
    - SSH to one of your nodes
    - ping between the nodes
    - transfer a file:
        - from your laptop to your XDC
		- from your laptop to your experimental node
		- from your XDC to your laptop
		- from your experimental node to your laptop
	- detach your experiment from your XDC
	- dematerialize your experiment
	- relinquish your realization
	- delete your XDC
	- delete your experiment


