# I2: Ansible and automation

## Overview

The SPHERE testbed software is a group of open source software packages that are deployed across heterogeneous systems, and are programmed and released in accordance with some common DevOps best practices. Ansible is a "configuration as code" tool, which allows us to configure once, repeat infinitely. This means that we can bring new machines, machines with replacement parts, or replacement machines online with minimal effort once we have successfully completed the configuration within the ansible tool.

### Definitions
- *Ansible*: A "Configuration as Code" tool written in python to automate configuration of machines/hosts/vms or any target which allows ssh connectivity and supports a python toolchain.
- **Ansible Role**: A collection of tasks and variables that are combined into a module for a specific purpose/tool/configuration change. Eseentially a good way of grouping related things together.
- **Ansible Galaxy**: the *official* ansible community code repository, but also the command that is utilized to both access the online community repos and to create boilerplate ansible roles.

## Goals
The goal of this project is to streamline the configuration and deployment of host configuration and software within the SPHERE ecosystem.

The configurations that we wish to pursue here are:
- Server OS level configurations
- Sofware configurations of software in use on our systems

A lofty objective here will be to modularize everything as much as possible into ansible "Roles" such that a role can be selectively applied to various hosts, groups, etc.

## Tasks

- [ ] Familiarize yourself with [ansible documentation](https://docs.ansible.com/)
- [ ] Familiarize yourself with the [MergeTB/SPHERE ansible code](https://gitlab.com/mergetb/ansible)
- [ ] Abstract/factor out re-usable bits & turn them into includes or "Roles"
- [ ] Test your ansible updates/changes on a VM.
- [ ] Ask the Ops team to help you deploy your code to our MergeTB/SPHERE ansible repo and apply to live machines
- [ ] (optional learning) Manually deploy "virtportal" on a host and configure a working virtual k8s cluster to play with

## Documentation

- Ansible
    - [ansible documentation](https://docs.ansible.com/)
    - [MergeTB/SPHERE ansible code](https://gitlab.com/mergetb/ansible)
