# I0: Release Engineering

## Overview

The SPHERE testbed software is a group of open source software packages that are deployed across heterogeneous systems, and are programmed and released in accordance with some common DevOps best practices. There are various resources and overviews of DevOps practices all over the internet: here is a relatively [concise](https://www.splunk.com/en_us/blog/learn/devops-release-management.html) one with a diagram.

### Definitions
- **CI/CD**: Continuous Integration, Continuous Deployment; The process of building/testing/deploying software packages in an automated way.
- **Code Repo**: A grouping of files with a journaled static hash of all contents. Used to track, review, process, and audit things like code or configuration files. Typically also a [git](https://github.com/git/git/blob/master/README.md) repository
- **Commit**: New journaled hashes in a code repo are called "commits" and represent a static state that can be referenced and acted upon.
- **Branch**: An organizational feature of a code repo which can be updated with commits independent of other branches. Can be thought of a little bit like a separate folder of commits, usually created to do independent work on the repo with the intention of a future merge.
- **Merge**: Merging two static commit states A into B entails incorporating the differences between them and resolving any conflicts in similar stanzas between the two commits in a code repo, with the resulting state being a new commit. Merges are usually carried out between branches.
- **Main/Master** and **Protected** branches: the main or master branch of a code repo is typically the reference branch and usually the landing page when someone initially visits or checks out the repo. Protected branches are branches that remain in a frozen state and cannot receive commits without special permission.
- **Merge Request/Pull Request**: When following DevOps best practices, the default and any other protected branches typically require a code review and approval of one or more additional team members before any merge of another branch or commit can be completed into them.
- **Tag**: An abstraction that allows addressing a branch or commit with another reference. Typically the reference is a Semantic Versioning number. Can be created manually or by CI/CD processes.
- **Release**: Under best practices the state of the default branch \(usually`main` or `master`\) branch of a code repo after a Merge or Pull request is completed. Can be created manually or by CI/CD processes.

## Goals
The goal of this project is to streamline the Dev to Ops transition of this process, to make sure that when a new Merge Request is approved and merged, a new "release" and all artifacts related to that release are created automatically.

This includes any:
- tags (which create ci/cd pipelines and build software with a semver tag)
- releases (a tarball with release notes)
- packages -- go binaries, rpm's or deb's (versioned based on the tag/semver)

This also includes notifications and planning for upgrades, which can potentially be placed in either a news announcement channel feed or a mattermost chat feed, and potentially even a discourse forum post. This can be discussed after the initial parts of the project are completed.

## Tasks

- [ ] Familiarize yourself with [gitlab ci](https://docs.gitlab.com/ee/topics/build_your_application.html)
- [ ] Look at our own internal repo structure
- [ ] Abstract/factor out re-usable bits & turn them into includes
- [ ] Test a new configuration of the .gitlab-ci.yml file to accomplish goals
- [ ] Deploy

## Documentation

- gitlab.yml
    - [more gitlab](https://docs.gitlab.com/ee/ci/yaml/#rules)
    - [more gitlab examples](https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html)
