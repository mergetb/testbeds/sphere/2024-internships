# I3: LLDP based facility modeling

## Overview

SPHERE is composed of a collection of _enclaves_, which consist of a set of interconnected resources
(servers, switches, and other types of devices). The resources that reside in an enclave are
commissioned against the Merge portal to make them available for experimentation.

Before resources can be commissioned, the owner needs to provide a formal description of what their
resources are, what they can do, and how they are connected. This project seeks to make this process
-- called the _facility modeling_ process -- easier and more automated by leveraging a network
protocol called LLDP (link layer discovery protocol). 

The goal of this project is to develop tools to run LLDP across a collection of devices and
switches, upload LLDP information to a database, and query the database to infer inter-device
connectivity information to facilitate automatic generation of facility models.

![](./i3-overview.png)

The image above diagrams the architecture of the system. TBD: add and describe figure

## Tasks

- [ ] Read up on LLDP and `lldpd`
- [ ] Request an account on the _mergelab_ Merge test environment
- [ ] Deploy a database to store lldp information on the _mergelab_ testbed
- [ ] Write a program that will deploy `lldpd` the _mergelab_ devices and upload data to the
  database
- [ ] Write a simple program that will query the database for connectivity information and summarize
  it
- [ ] Write a program that will query the database and produce a Merge
  [XIR](https://gitlab.com/mergetb/xir) model
- [ ] Stretch goal: extend the Merge [ground-control](https://gitlab.com/mergetb/ops/ground-control)
  to run that lldp database 

## Documentation

- LLDP documentation:
    - [Wikipedia page](https://en.wikipedia.org/wiki/Link_Layer_Discovery_Protocol)
    - [lldpd](https://lldpd.github.io/)
- [MergeTB XIR source code](https://gitlab.com/mergetb/xir) - written protobuf
    - Google protocol buffers documentation: https://protobuf.dev/
