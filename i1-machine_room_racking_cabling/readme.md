# I2: Ansible and automation

## Overview

The SPHERE testbed software is a group of open source software packages that are deployed across heterogeneous systems. The SPHERE infrastructure is replacing an old DETER infrastructure at the command and control, networking, and the node/endpoint level. In order to do this, old systems must be removed and e-wasted, and new systems must be racked, cabled, and configured.

### Definitions
- **Rack**: a vertical 2/4 post or full enclosure in which to mount server hardware, ideally via cage nuts and accompanying bolts/screws and non-ideally directly with screws that correctly fit the racks' screw holes.

## Goals
The primary goal of this project is to clear the old hardware and make way for new hardware in the racks that previously hosted DETER systems.

This includes removing and e-wasting any hardware and cables, notifying purchasing of ISI asset tags of any e-waste that is placed in the mezzanine e-waste cage, and moving any new hardware into position to be ready for the next generation of hardware.

## Tasks

- [ ] Completely remove all old hardware
- [ ] Harvest parts from old hardware (ConnectX NICs)
- [ ] Rack up new server hardware for the SPHERE portal
- [ ] Rack up **existing** switches for the new SPHERE `generalserver` enclave/facility
- [ ] Rack up current-generation power for the new `generalserver` enclave/facility
- [ ] (optional learning) Manually remotely deploy a server node on F38

## Documentation

- Ansible
    - Nvidia/Mellanox [SN3700](https://docs.nvidia.com/networking/display/sn3000um/ordering+information) documentation (experiemnt network leaves)
    - Nvidia/Mellanox [SN4600C/4600V/4700](https://docs.nvidia.com/networking/display/sn4000/ordering+information) documentation
    - "White Box" Networking OS [Cumulus Linux](https://docs.nvidia.com/networking-ethernet-software/cumulus-linux-58/) documentation
    - Edge-Core [AS4610](https://www.nvidia.com/en-us/networking/ethernet-switching/as4610/) Open Switch  Hardware overview, and [manufacturer's info](https://www.edge-core.com/product/as4610-54t/) site.
