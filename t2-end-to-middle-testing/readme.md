# "End to Middle" testing for Merge Testbeds

## Overview

Testing is important. It is often overlooked in a research or academic institution like ISI as programs are time limited and often geared toward the 
research and not the software that is written to support the research. The Merge Testbed software is different though. ISI has a history of 
running and managing cyber-security testbeds as research infrastructure. The major testbed software used by ISI before Merge, was called DETER and was in
constant use for 20 years. Given this, we want to treat the Merge project as a long term, sustainable software project and testing is an indispensable 
aspect of a good, robust, and mature software project. Testing covers new code changes but also, and probably more importantly, it covers
[regressions](https://en.wikipedia.org/wiki/Regression_testing) which ensure that new changes do not change or break existing behavior. 

This task adds "end to middle" testing to Merge. Merge has three basic parts (as you can see in the [Merge Overview documentation](https://mergetb.org/docs/concepts/overview/): 
an experimenter using the Merge API, a Merge Portal, and testbed(s) hardware itself. The workflow is an experimenter interacts with the Portal via an API and then Portal interacts with a
testbed or testbeds. This task focus on the first part: the interaction between the Merge API (driven by the experimenter) and the Merge Portal. It's "end to middle" as it
leaves out the Portal to testbed interaction. This is a good place to start testing as we can leave out the complicated and unique nature of the testbed and focus on the standardized
interaction between the Merge API and the Merge Portal. Also it requires many fewer actual resources. You can stand up a portal and client software on a single moderately powerful
machine. No testbed with many machines and networks required. 

## Tasks

- [ ] If you do not know Go, [pick up the basics](https://go.dev/tour/welcome/1). Most Merge software is written in Go.
- [ ] Familiarize yourself with relevant portions of the [Merge Testbed software](https://gitlab.com/mergetb)
  - [ ] [Merge Poral Services](https://gitlab.com/mergetb/portal/services)
  - [ ] [The Merge API](https://gitlab.com/mergetb/api)
    - [ ] It may be easier to start with the [documentation generated automatically](https://mergetb.gitlab.io/api/) from the Merge API in that repository.
- [ ] Familiarize yourself with the existing unit tests in the Portal Services to get a feel for testing code in Go
  - [ ] [pkg/workspace](https://gitlab.com/mergetb/portal/services/-/blob/main/pkg/workspace/workspace_test.go)
  - [ ] [pkg/storage](https://gitlab.com/mergetb/portal/services/-/blob/main/pkg/storage/storage_test.go)
- [ ] Familiarize yourself with the command line client.
  - [ ] Run `mrg` against a portal to get a feel for it.
  - [ ] Review the [`mrg` code base](https://gitlab.com/mergetb/portal/cli) as a good example of a client using the Merge API to interact with a Merge Portal 

- [ ] Talk to Geoff about [the existing rudimentary end-to-middle testing code](https://gitlab.com/mergetb/portal/testing/-/tree/main/test?ref_type=heads) and strategies that currently exist.
  - [ ] Work with him to extend them.

## Documentation

Links above contain most of the documentation and code you will need. But here are a few useful links about Merge.
- [Merge Documentation](https://mergetb.org/docs/experimentation/)
- [Merge Walk-through](https://mergetb.org/docs/experimentation/hello-world-gui/)

## Contact Info
* glawler@isi.edu
* glawler on Mattermost
