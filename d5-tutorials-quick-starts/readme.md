# Add Tutorials and/or Quick Starts to Merge Launch

## Overview

Merge Launch is the graphical user interface (GUI) to a testbed cluster running Merge. The GUI lets users register for accounts, authenticate themselves to the portal, 
create/delete/update things for their accounts. In Launch there are links to documentation including a "walk-through" with text and images. It would be good to 
have a more interactive way to do tutorials or walk-throughs given it's a GUI and is by it's nature interactive. 

Launch is written in Javascript and Typescript and uses React. It also uses the Patternfly framework which has built-in support for 
[Quick Starts](https://www.patternfly.org/extensions/quick-starts). This task is adding Quick Starts to Launch. 

Note that ultimately this task is about making the experience using Launch easier and more friendly. If you know or develop or just come across other techniques that you
think would make Launch a more effective platform, please let us know. This task is fungible and we are very much willing to listen to ideas. You are in a 
great spot to help us with this as you do not yet have experience with Launch. You have [beginner's mind](https://en.wikipedia.org/wiki/Shoshin). 

## Tasks

- [ ] Familiarize yourself with the [Launch code base](https://gitlab.com/mergetb/portal/launch)
  - [ ] Familiarize yourself with the libraries and toolkits that Launch uses
     - [ ] [React](https://react.dev/)
     - [ ] [PatternFly Quick Start Extension](https://v4-archive.patternfly.org/v4/extensions/quick-starts)
     - [ ] The [Merge API](https://mergetb.gitlab.io/api/) - how Launch talks to Merge
- [ ] Run an development instance of Launch against an active Portal - this is where you will be doing your work

- [ ] Analyze existing workflows in Launch to determine which are the most common or needlessly complex
- [ ] Choose a few work flows to document via Quick Starts or Tutorials
  - [ ] Implement the Quick Starts or write/document the tutorials
  - [ ] Test. 
    - [ ] One of your fellow interns is working on automated GUI testing. Once you have a working Quick Start, reach out to them and 
    collaborate on testing of your new components. 

## Documentation

* [Merge Testbed Source Code](https://gitlab.com/mergetb). There is a lot there. You are not expected to know it all or even be familiar with it all.
* [Launch Source Code](https://gitlab.com/mergetb/portal/launch)
* [Patternfly Documentation (V4)](https://v4-archive.patternfly.org/v4/) (Note that Launch uses Patternfly v4. The newest version is v5.) 
* [Patternfly Quick Starts](https://v4-archive.patternfly.org/v4/extensions/quick-starts)

## Contact Info
glawler@isi.edu
glawler on Mattermost
