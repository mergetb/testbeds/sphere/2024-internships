# NAME

## Overview

SPHERE is built using Merge, a testbed software platform developed at USC/ISI. Merge provides an application programming interface (API) that can be invoked by external clients to orchestrate activity on the SPHERE infrastructure.

Merge implments this via a microservice architecture. In theory, it should be resilient to partial service disruptions, but in practice, all sorts of weird race conditions or failed preconditions can happen if certain services or machines are down or take too long.

The goal of this project then, is to find out what exactly breaks when certain services or machines are down or take too long. Obviously, if a service/machine is down, it won't be responsive, but hopefully, no weird system-level bugs/inconsistency occur when a service/machine is down and even better, when such disruptions occurs, users have some visibility into why something is misbehaving.

## Tasks

- [ ] Bootstrapping things (accounts, access to development resources, etc.)
- [ ] Familiarize yourself with microservice architecture (aka, what services are where, and what does what)
  - [ ] Producing a "flow" graph of service inputs and outputs and how things flow would be useful
- [ ] Loop:
  - [ ] Try a workflow
  - [ ] Introduce chaos (turning machines/services off, flooding the network, etc.)
  - [ ] Try the workflow again
  - [ ] Document deviations in behavior
- [ ] (Very stretch) If you're feeling really confident, you can implement a fix

## Documentation

* [Merge Source Code](https://gitlab.com/mergetb/portal/services)
* The Portal uses Kubernetes
* The Facility has several backends, so depending on the exact resources, it could be either virtualized or a few actual machines

## Contact Info
christra@isi.edu
christran on mattermost
