# D0: Class UI

## Overview

The goal of this project is to create class UI on SPHERE that resembles the one on old DeterLab, so the old UI
can be retired. 

## Tasks

* if a user is member of class project, create tab "My Classes" (Student) or "Teaching" (teacher/TA) that is visible to them
  When that tab is active show list of classes that they can click on to see more info
* teacher view: create a new page to enroll students. Teacher pastes list of emails, one per line and selects Student or TA role
  Then clicks "Register" and all are registered
* teacher view: create a new page to show list of current students. Each student can be deleted or their password changed by clicking on a button
* teacher view: teachers can adopt new materials to their class by giving a URL to the material. This copies that folder to their /organization 
  or to their account, somewhere where they can change it. Once they publish the material it is visible to all students.
* teacher view: teachers can assign materials to students in class, either all or individually by student. Assignments have deadlines that can be
  extended by a teacher. Assignments can be also deleted
* teacher view: teachers can download submissions for assignments
* student view: students can click on a class in "My Classes" list and see list of published materials and list of assignments and their deadlines
* student view: students can click on an assignment and upload their submission. It gets stored on our system, perhaps in /organization but is only
  readable by the teacher and TAs

## Documentation

* [Code](https://github.com/deter-project/testbed) - private, send Jelena your Github login to be added to repo
* [Documentation](https://docs.deterlab.net/support/class/)



