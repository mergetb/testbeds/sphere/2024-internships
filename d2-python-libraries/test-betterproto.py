from mrglib.portal.v1 import IdentityStub, LoginRequest
from mrglib.portal.v1 import WorkspaceStub, GetProjectsRequest, GetProjectExperimentsRequest

import ssl
import asyncio
import sys
from grpclib.client import Channel

async def main(username, password):
    async with Channel("grpc.mod.deterlab.net", port=443, ssl=True) as channel:
        ids = IdentityStub(channel)
        response = await ids.login(LoginRequest(username=username, password=password))
        token = response.token

        wss = WorkspaceStub(channel)
        response = await wss.get_projects(GetProjectsRequest(), metadata={
            "authorization": "Bearer " + token,
        })

        for proj in response.projects:
            print('Project: %s' % proj.name)

            response = await wss.get_project_experiments(GetProjectExperimentsRequest(project=proj.name), metadata={
                "authorization": "Bearer " + token,
            })
            for exp in response.experiments:
                print('  Experiment: %s' % exp.name)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: <username> <password>")
        sys.exit(1)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main(sys.argv[1], sys.argv[2]))
