# D2: Merge Python libraries and documentation

## Overview

SPHERE is built using Merge, a testbed software platform developed at USC/ISI. Merge provides an application programming interface (API) that can be invoked by external clients to orchestrate activity on the SPHERE infrastructure.

The goal of this project is to develop a Python library which sends commmands to the Merge portal via the Merge API. The library will call out to the Merge API over the Internet using the Merge API's GRPC interface. This project will also develop documentation for the library and examples showing how to use it

![](./d2-overview.png)

The image above diagrams the architecture of the system. The Merge API can be invoked via either the
REST or GRPC interface. Currently, two Merge tools exist which use these interfaces: The
[launch](https://gitlab.com/mergetb/portal/launch) graphical user interface (GUI) uses the REST
interface, while the [mrg command line client](https://gitlab.com/mergetb/portal/cli) (CLI) used the GRPC
interface. This project will develop a 3rd client (LIB) will also use the GRPC interface.

## Tasks

- [ ] Connect to sphered2.ads.isi.edu via SSH (ask Brian to create an account for you)
- [ ] Download and run docker build container
- [ ] Compile the Python library using the Merge GRPC API bindings
- [ ] Compile a test program against the Python library
- [ ] Develop classes that wrap the auto-generated GRPC API bindings
- [ ] Write documentation for the library
- [ ] Write example use-cases for the library; e.g.,
    - [ ] how to login to the Merge portal
    - [ ] how to create a project
    - [ ] how to create an experiment
    - [ ] how to load a topology into an experiment revision
    - [ ] how to realize an experiment
    - [ ] how to materialize an experiment

## Documentation

- [MergeTB API source code](https://gitlab.com/mergetb/api/-/tree/main?ref_type=heads) - written in protobuf
    - Google protocol buffers documentation: https://protobuf.dev/
- [Betterproto](https://pypi.org/project/betterproto/) - protobuf-to-python compiler used to generate Merge API GRPC bindings
- [build-container](https://gitlab.com/mergetb/testbeds/sphere/internships/tools/-/tree/main/build-container)
  - Docker container with Merge build tools

## Other Materials

### mrglib initial attempt
We previously started work on `mrglib` here: https://github.com/STEELISI/mrglib/tree/main. This may
be useful to show what we are envisioning. Note that this was a work in progress, and simply made
calls to run the `mrg` CLI rather than invoking the Merge API directly through the GRPC interface.

### Python API bindings

Download this
[mrglib.tgz](https://gitlab.com/-/project/27402360/uploads/7a9080b3bea7acc01f8f6230c62fc43d/mrglib.tgz)
which includes auto-generated Python bindings for the Merge API. With this python package you can
call out to the Merge API via the GRPC interface.

### Example

Download [test-betterproto.py](./test-betterproto.py) for an example of how to use the
auto-generated Python bindings. 

Putting it all together, these steps should work (adding your Merge username/password) for the last
command:
```
curl -o mrglib.tgz -L https://gitlab.com/-/project/27402360/uploads/7a9080b3bea7acc01f8f6230c62fc43d/mrglib.tgz
tar xvf mrglib.tgz
curl -OL https://gitlab.com/mergetb/testbeds/sphere/internships/2024-internships/-/raw/main/d2-python-libraries/test-betterproto.py
python3 -u test-betterproto.py <username> <password>
```
