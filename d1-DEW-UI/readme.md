# D1: DEW UI

## Overview

The goal of this project is to enable  DEW UI, which resides [here](https://dew.isi.edu) to work as graphical user interface for
experiment design and running on SPHERE.

## Tasks

* remove Google login
* enable using of SPHERE as identity provider so users logged into SPHERE can use DEW UI without further logging in
* generate SPHERE-like topologies (this may be already completed, small fixes may be needed)
* link to SPHERE for experiment execution

## Documentation

* [Code](https://github.com/STEELISI/DEW) - private, send Jelena your Github login to be added to repo
* [Documentation](https://dew.isi.edu/docs/)
* [YouTube videos](https://www.youtube.com/channel/UCocyRn8Lk40f_1giKrDnRLQ) - showcasing how DEW worked with DeterLab


