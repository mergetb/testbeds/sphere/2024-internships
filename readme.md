# 2024 Internships

## Project Descriptions

Development Projects:
- [D0: Class UI](./d0-CLASS-UI) - Jelena
- [D1: DEW UI](./d1-DEW-UI) - Jelena
- [D2: Merge Python libraries and documentation](./d2-python-libraries) - Brian
- [D3: Traffic monitoring](./d3-firewall-traffic-monitoring) - Chris
- [D4: Launch revamp](./d4-launch-revamp) - Chris
- [D5: Tutorials/quick-starts](./d5-tutorials-quick-starts) - Geoff

Infrastructure Projects:
- [I0: Release engineering](./i0-Release_Engineering) - Joe
- [I1: Machine room](./i1-machine_room_racking_cabling) - Joe
- [I2: Infrastructure automation](./i2-automation_ansible_etc) - Joe
- [I3: LLDP based facility modeling](./i3-lldp) - Brian

Testing Projects:
- [T0: Automated GUI testing](./t0-automated-gui-testing) - Geoff
- [T1: Chaos testing](./t1-chaos-testing) - Chris
- [T2: End-to-middle testing](./t2-end-to-middle-testing) - Geoff

## Project Assignments

- Watson: D0, D1
- Fatima: D2
- Mwamba: D3
- Oweh: D4, T1
- Chavan: D5, T0
- Chen: I0
- Vincent: I1, I2
- Bajracharya: I3
- Medina: T2