# NAME

## Overview

SPHERE is a network testbed, built on top of Merge. As a network testbed, firewalls and monitoring is good(TM). While a fair amount of this is done operationally as the network testbed is connected to the bed of chaos known as the internet, there's also a need for monitoring and firewalls for testbed and experimenter use.

Right now, an experimenter can request a certain amount of bandwidth from the system. However, there's no guarantees that they actually get the bandwidth they requested, where depending on testbed use, it's possible that they end up getting less bandwidth than they requested. So for this part, the goal would be to implement something that would ultimately be able to tell an experimenter that their experiment had sufficient resources, or as a less strong proposition, to be able to tell experimenters if network resources were ever overwhelmed, and when.

For firewalls, if the experimenter has an idea that they might be doing something crazy, the experimenter can tell Merge ahead of time so that Merge can set up appropriate firewall rules for their experiment.

## Tasks

- [ ] Bootstrapping things (accounts, access to development resources, etc.)
- [ ] Get familiar with cumulus, the switches
- [ ] Implement network monitoring across the network
  - [ ] You probably shouldn't do this entirely from scratch, there's probably programs out there that can help you implement this
  - [ ] Based on some threshold of performance or use, generate alerts
- [ ] Implement something that can dynamically configure a firewall
  - [ ] As much of Merge is middleware, this is essentially creating a firewall middleware service for Merge

## Documentation

* [Merge Source Code](https://gitlab.com/mergetb/portal/services)
* [MergeTB Facility Overview](https://mergetb.org/docs/facility/overview/)
  - One of the diagrams there is a general overview of how a Merge facility testbed is connected and organized

## Contact Info
christra@isi.edu
christran on mattermost
